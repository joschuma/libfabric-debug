LIBFABRIC_BASE = /nfs/sw/felix/fabric/build
INCLUDE = -I $(LIBFABRIC_BASE)/include
LIB = -L $(LIBFABRIC_BASE)/lib -l fabric


all: listen connect

listen: listen.cpp
	g++ -o $@ $< -g -O0 $(INCLUDE) $(LIB)

connect: connect.cpp
	g++ -o $@ $< -g -O0 $(INCLUDE) $(LIB)
