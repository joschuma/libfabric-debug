#include <sstream>
#include <iostream>
#include <cstdlib>

#include <rdma/fabric.h>
#include <rdma/fi_eq.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_errno.h>

#define MAX_CQ_ENTRIES (128)

#define ERROR(msg, c)						\
  do {								\
    std::stringstream s;					\
    s << msg << ": " << fi_strerror(-c) << " (" << -c << ")";	\
    exit(1);							\
  } while(0)


int main(int argc, char** argv)
{
  struct fi_info* fi;
  struct fid_fabric *fabric;
  struct fid_eq *eq;
  struct fid_domain *domain;
  struct fid_ep *ep;
  struct fid_cq* cq;

  int eqfd;
  int ret;
  struct fi_info* hints;
  struct fi_eq_attr eq_attr;
  eq_attr.wait_obj = FI_WAIT_UNSPEC; // waiting only through fi_ calls (no epoll)
  
  hints = fi_allocinfo();
  hints->ep_attr->type  = FI_EP_MSG;
  hints->caps = FI_MSG;
  hints->mode   = FI_LOCAL_MR;
  hints->domain_attr->data_progress = FI_PROGRESS_AUTO;
  hints->domain_attr->resource_mgmt = FI_RM_ENABLED;
  
  std::cout << "connecting to " << argv[1] << ":" << argv[2] << std::endl;
  
  if((ret = fi_getinfo(FI_VERSION(1, 1), argv[1], argv[2], 0, hints, &fi)))
    {
      ERROR("fi_getinfo failed", ret);
    }
  
  if((ret = fi_fabric(fi->fabric_attr, &fabric, NULL)))
    {
      ERROR("fi_fabric failed", ret);
    }
  
  if((ret = fi_eq_open(fabric, &eq_attr, &eq, NULL)))
    {
      ERROR("fi_eq_open failed", ret);
    }
  
  if((ret = fi_domain(fabric, fi, &domain, NULL)))
    {
      ERROR("fi_domain failed", ret);
    }
  
  if((ret = fi_endpoint(domain, fi, &ep, NULL)))
    {
      ERROR("fi_endpoint failed", ret);
    }
  
  if((ret = fi_ep_bind((ep), &eq->fid, 0)))
    {
      ERROR("fi_ep_bind failed", ret);
    }
  
  struct fi_cq_attr cq_attr;
  cq_attr.size = MAX_CQ_ENTRIES;      /* # entries for CQ */
  cq_attr.flags = 0;     /* operation flags */
  cq_attr.format = FI_CQ_FORMAT_CONTEXT;    /* completion format */
  cq_attr.wait_obj= FI_WAIT_UNSPEC;  /* requested wait object */
  cq_attr.signaling_vector = 0; /* interrupt affinity */
  cq_attr.wait_cond = FI_CQ_COND_NONE; /* wait condition format */
  cq_attr.wait_set = NULL;  /* optional wait set */
  
  if((ret = fi_cq_open(domain, &cq_attr, &cq, NULL)))
    {
      ERROR("fi_cq_open failed", ret);
    }
  
  if((ret = fi_ep_bind((ep), &cq->fid, FI_TRANSMIT|FI_RECV)))
    {
      ERROR("fi_ep_bind failed", ret);
    }
  
  /* Connect to server */
  if((ret = fi_connect(ep, fi->dest_addr, NULL, 0)))
    {
      ERROR("fi_connect failed", ret);
    }
  
  uint32_t event;
  struct fi_eq_cm_entry entry;

  ssize_t rd = fi_eq_sread(eq, &event, &entry, sizeof entry, -1, 0);
  return 0;
}
