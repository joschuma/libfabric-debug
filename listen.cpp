#include <sstream>
#include <iostream>
#include <cstdlib>

#include <rdma/fabric.h>
#include <rdma/fi_eq.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_errno.h>


#define ERROR(msg, c)						\
  do {								\
    std::stringstream s;					\
    s << msg << ": " << fi_strerror(-c) << " (" << -c << ")";	\
    exit(1);							\
  } while(0)



int main(int argc, char** argv)
{
  int ret;
  struct fi_info* hints;
  struct fi_eq_attr eq_attr;
  eq_attr.wait_obj = FI_WAIT_UNSPEC;

  hints = fi_allocinfo();
  hints->ep_attr->type  = FI_EP_MSG;
  hints->caps = FI_MSG;
  hints->mode = FI_LOCAL_MR;

  struct fi_info* fi;
  struct fid_fabric *fabric;
  struct fid_eq *eq;
  struct fid_pep *pep;

  int evq_fd;

  if((ret = fi_getinfo(FI_VERSION(1, 1), "0.0.0.0", "12345", FI_SOURCE, hints, &fi)))    { ERROR("fi_getinfo failed", ret); }
  if((ret = fi_fabric(fi->fabric_attr, &fabric, NULL)))    { ERROR("fi_fabric failed", ret); }
  if((ret = fi_eq_open(fabric, &eq_attr, &eq, NULL)))    { ERROR("fi_eq_open failed", ret); }
  if((ret = fi_passive_ep(fabric, fi, &pep, NULL)))    { ERROR("fi_passive_ep failed", ret); }
  if((ret = fi_pep_bind(pep, &eq->fid, 0)))    { ERROR("fi_pep_bind failed", ret); }
  if((ret = fi_listen(pep)))    { ERROR("fi_listen failed", ret); }
  
  // wait for event on event queue
  std::cerr << "waiting for connection..." << std::endl;

  uint32_t event;
  struct fi_eq_cm_entry entry;

  ssize_t rd = fi_eq_sread(eq, &event, &entry, sizeof entry, -1, 0);
  if(rd < 0)
    {
      if(rd == -FI_EAVAIL)
        {
	  int r;
	  struct fi_eq_err_entry err_entry;
	  if((r = fi_eq_readerr(eq, &err_entry, 0)) < 0)
            {
	      ERROR("fi_eq_readerr failed", r);
            }
	  std::cerr << "error in the CQ: " << fi_strerror(err_entry.err) << "/" << fi_strerror(err_entry.prov_errno);
        }
      ERROR("fi_eq_sread failed", rd);
    }
  if (rd != sizeof entry)
    {
      ERROR("reading from event queue failed: rd=%d", rd);
    }
  

  std::cerr << "Successfully received an event: ";

  switch (event)
    {
    case FI_CONNREQ:
      std::cout << "FI_CONNREQ" << std::endl;
      break;

    case FI_CONNECTED:
      std::cout << "FI_CONNECTED" << std::endl;
      break;

    case FI_SHUTDOWN:
      std::cout << "FI_SHUTDOWN" << std::endl;
      break;

    default:
      std::cout << "unknown event: " << event << std::endl;
      break;
    }

  return 0;
}

